<div class="page-all-articles">
	<? include 'inc/modules/breadcrumbs.php';?>
	<!-- / -->
	<div class="head">
		<div class="container">
			<div class="grid">
				<div class="cell-8 shift-2">
					<h1 class="title">Все статьи</h1>
					<p>Платные медицинские услуги медцентра Верамед. Наш принцип — изо всех возможных методов лечения выбрать для пациента именно тот, который даст максимальную пользу.</p>
				</div>
			</div>
		</div>
	</div>
	<!-- / -->
	
	<div class="all-articles-list">
		<div class="filter">
			<div class="container">
				<div class="place">
					<div class="search">
						<input type="text" placeholder="Поиск по тегам">
					</div>
					<div class="popular-tags">
						<p>Популярные теги</p>
						<div class="tags">
							<ul>
								<li><a href="#">Акушер-гинеколог</a></li>
								<li><a href="#">Уролог</a></li>
								<li><a href="#">Периантолог</a></li>
								<li><a href="#">Иглорефлексотерапевт</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="list">
			<div class="container">
				<div class="list-place">
					<?for ($i=0; $i < 6; $i++) { ?>
					<div class="item">
						<div class="place">
							<div class="level-0" style="background-image: url(/project/images/other/prog-1.jpg);">
								<div class="type">Статьи</div>
								<div class="name"><a href="#">Участвуй во всероссийском проекте «Проверь печень»</a></div>
							</div>
							<div class="level-1">«Первоочередной замысел всероссийского спецпроекта «Проверь печень» — подготовка и исследование данных о распространённости ... </div>
							<div class="level-2">
								<div class="tags">
									<ul>
										<li>Акушер-гинеколог</li>
										<li>Уролог</li>
										<li>Перинаталог</li>
										<li>Иглорефлексотерапевт</li>
									</ul>
								</div>
							</div>
							<div class="level-3">
								<a href="#">Подробнее</a>
							</div>
						</div>
					</div>
					<?}?>
				</div>
				<div class="show-more">
					<div class="button">Показать больше</div>
				</div>
				<div class="paginator">
					<div class="to-start">
						<span>В начало</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	
	<!-- / -->
	<? include 'inc/modules/news-block.php';?>
	

	<? include 'inc/modules/benefit.php';?>
	<!-- / -->
	<? include 'inc/modules/navigation.php';?>
</div>