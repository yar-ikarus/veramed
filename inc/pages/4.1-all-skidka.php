<div class="page-all-skidka">
	<? include 'inc/modules/breadcrumbs.php';?>
	<!-- / -->
	<div class="head">
		<div class="container">
			<h1 class="title">Акции</h1>
			<div class="grid">
				<div class="cell-10 shift-1">
					<p>Платные медицинские услуги медцентра Верамед. Наш принцип — изо всех возможных методов лечения выбрать для пациента именно тот, который даст максимальную пользу.</p>
				</div>
			</div>
		</div>
	</div>

	<div class="all-skidka">
		<div class="container">
			<div class="grid">
				<div class="cell-8 shift-2">
					<h2 class="title">Новые программы</h2>
					<div class="filter">
						<div class="group">
							<div class="cell">
								<select name="">
									<option value="">Клиника</option>
									<option value="">Клиника</option>
									<option value="">Клиника</option>
									<option value="">Клиника</option>
								</select>
							</div>
							<div class="cell switcher-place">
								<div class="switcher off"></div>
								<span>Детская программа</span>
							</div>
							<div class="cell">
								<div class="button"><span>Поиск</span></div>
							</div>
						</div>
					</div>
					<div class="skidka-result-place">
						<? for ($i=0; $i < 3; $i++) { ?>
							<div class="item" style="background-image: url(/project/images/other/action-bg-1.jpg);">
								<div class="level-0">Акции</div>
								<div class="level-1">
									<div class="name"><a href="#">Реабилитационный физиотерапевтический комплекс "аденоиды и риносинуситы"</a></div>
									<div class="date">с 19 сентября по 20 октября</div>
								</div>
								<div class="level-2">
									<div class="points">
										<div class="point">
											<span>ВЕРАМЕД</span>
											<span>Одинцово</span>
										</div>
										<div class="point">
											<span>ВЕРАМЕД</span>
											<span>Звенигород</span>
										</div>
										<div class="point">
											<span>ВЕРАМЕД</span>
											<span>Премиум</span>
										</div>
									</div>
									<div class="more">
										<a href="#">Подробнее</a>
									</div>
								</div>
							</div>
						<?}?>
					</div>
					<div class="show-more">
						<div class="button">Показать больше</div>
					</div>
					<div class="paginator">
						<div class="to-start">
							<span>В начало</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	
	
	<!-- / -->
	<? include 'inc/modules/benefit.php';?>
	<!-- / -->
	<? include 'inc/modules/reviews.php';?>
	<!-- / -->
	<? include 'inc/modules/navigation.php';?>
	<!-- / -->
	<? include 'inc/modules/seo-block.php';?>
</div>