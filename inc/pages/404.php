<div class="page-404">
	<!-- / -->
	<div class="head">
		<div class="container">
			<h1 class="title">404</h1>
			<div class="grid">
				<div class="cell-10">
					<div class="alert">страница не найдена :(</div>
					<p>К сожалению мы не смогли найти страницу по вашему запросу, попробуйте воспользоваться поиском </p>
					<div class="search">
						<input type="text" placeholder="Поиск">
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- / -->
	
</div>