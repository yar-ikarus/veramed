<div class="page-all-programming">
	<? include 'inc/modules/breadcrumbs.php';?>
	<!-- / -->
	<div class="head">
		<div class="container">
			<h1 class="title">Курс ФизиоТерапевтической реабилитации</h1>
			<div class="grid">
				<div class="cell-10 shift-1">
					<p>Платные медицинские услуги медцентра Верамед. Наш принцип — изо всех возможных методов лечения выбрать для пациента именно тот, который даст максимальную пользу.</p>
				</div>
			</div>
		</div>
	</div>

	<div class="all-programs">
		<div class="container">
			<div class="grid">
				<div class="cell-8 shift-2">
					<h2 class="title">Новые программы</h2>
					<div class="filter">
						<div class="group">
							<div class="cell">
								<select name="">
									<option value="">Клиника</option>
									<option value="">Клиника</option>
									<option value="">Клиника</option>
									<option value="">Клиника</option>
								</select>
							</div>
							<div class="cell">
								<div class="switcher off"></div>
								<span>Детская программа</span>
							</div>
							<div class="cell">
								<div class="button"><span>Поиск</span></div>
							</div>
						</div>
					</div>
					<div class="programs-place">
						<? for ($i=0; $i < 3; $i++) { ?>
						<div class="item">
							<div class="img" style="background-image: url(/project/images/other/programm_1.jpg);">
								<a href="#"></a>
							</div>
							<div class="place">
								<div class="level-0">Программа</div>
								<div class="level-1">
									<div><a href="#">Реабилитационные физиотерапевтические комплексы</a></div>
									<div>
										<span>25.000 - <br>50.000 Р</span>
									</div>
								</div>
								<div class="level-2">
									<ul>
										<li><span>Только качественные методы лечения</span></li>
										<li><span>Профессиональные врачи и персонал клиники</span></li>
									</ul>
								</div>
								<div class="level-3">
									<div class="more"><a href="#">Подробнее</a></div>
									<div class="tags">
										<div class="tag">
											<img src="/project/images/icons/prog-tag-1.png">
											<span>Для женщин</span>
										</div>
										<div class="tag">
											<img src="/project/images/icons/prog-tag-2.png">
											<span>Горячие новинки</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?}?>
					</div>
					<div class="show-more">
						<div class="button">Показать больше</div>
					</div>
					<div class="paginator">
						<div class="to-start">
							<span>В начало</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="programs-block">
		<div class="container">
			<h2 class="title">Программы для всей семьи</h2>
			<div class="programs-place">
				<a href="#" class="item">
					<div class="place" style="background-image: url(/project/images/other/prog-1.jpg);">
						<div class="level-0">Программы</div>
						<div class="level-1">Курс физиотерапевтической реабилитации</div>
						<div class="level-2">Важное описание баннер программы с событиями, которые есть</div>
						<div class="level-3"><span>Подробнее</span></div>
					</div>
				</a>
				<a href="#" class="item">
					<div class="place" style="background-image: url(/project/images/other/prog-1.jpg);">
						<div class="level-0">Программы</div>
						<div class="level-1">Курс физиотерапевтической реабилитации</div>
						<div class="level-2">Важное описание баннер программы с событиями, которые есть</div>
						<div class="level-3"><span>Подробнее</span></div>
					</div>
				</a>
				<a href="#" class="item">
					<div class="place" style="background-image: url(/project/images/other/prog-3.jpg);">
						<div class="level-0">Программы</div>
						<div class="level-1">Курс физиотерапевтической реабилитации</div>
						<div class="level-2">Важное описание баннер программы с событиями, которые есть</div>
						<div class="level-3"><span>Подробнее</span></div>
					</div>
				</a>
				<a href="#" class="item">
					<div class="place" style="background-image: url(/project/images/other/prog-4.jpg);">
						<div class="level-0">Программы</div>
						<div class="level-1">Курс физиотерапевтической реабилитации</div>
						<div class="level-2">Важное описание баннер программы с событиями, которые есть</div>
						<div class="level-3"><span>Подробнее</span></div>
					</div>
				</a>
				<a href="#" class="item">
					<div class="place" style="background-image: url(/project/images/other/prog-5.jpg);">
						<div class="level-0">Программы</div>
						<div class="level-1">Курс физиотерапевтической реабилитации</div>
						<div class="level-2">Важное описание баннер программы с событиями, которые есть</div>
						<div class="level-3"><span>Подробнее</span></div>
					</div>
				</a>
			</div>
		</div>
	</div>

	
	<!-- / -->
	<? include 'inc/modules/timeline.php';?>
	<!-- / -->
	<? include 'inc/modules/benefit.php';?>
	<!-- / -->
	<? include 'inc/modules/reviews.php';?>
	<!-- / -->
	<? include 'inc/modules/navigation.php';?>
	<!-- / -->
</div>