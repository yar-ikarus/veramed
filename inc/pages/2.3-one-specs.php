<div class="page-one-specs">
	<? include 'inc/modules/breadcrumbs.php';?>
	<!-- / -->
	<div class="head">
		<div class="container">
			<h1 class="title">Константинопольский К.К.</h1>
			<div class="grid">
				<div class="cell-10 shift-1">
					<p>Платные медицинские услуги медцентра Верамед. Наш принцип — изо всех возможных методов лечения выбрать для пациента именно тот, который даст максимальную пользу.</p>
				</div>
			</div>
		</div>
	</div>
	<!-- / -->
	<div class="doctor-card">
		<div class="container">
			<div class="grid">
				<div class="cell-3 image">
					<div class="img" style="background-image: url(/project/images/other/doctor-demo.jpg);"></div>
					<div class="button">
						<div class="btn blue get-register">Записаться на прием</div>
					</div>
				</div>
				<div class="cell-6 info">
					<div class="level-0">
						<div class="group">
							<div class="cell name">
								<div>Константинопольский</div>
								<div>Константин Константинович</div>
							</div>
							<div class="cell skills">
								<div class="skill">
									<div>
										<i class="icons-doctor-1"></i>
									</div>
									<div>Прием <br>взрослых</div>
								</div>
								<div class="skill">
									<div>
										<i class="icons-doctor-2"></i>
									</div>
									<div>Прием детей <br>с 0 лет</div>
								</div>
							</div>
						</div>
					</div>
					<div class="level-1">
						<div class="spez main">
							<p>Основная специализация: </p>
							<ul>
								<li>Акушер-гинеколог</li>
								<li>Уролог</li>
								<li>Перинаталог</li>
								<li>Иглорефлексотерапевт</li>
							</ul>
						</div>
						<div class="spez">
							<p>Дополнительная специализация: </p>
							<ul>
								<li>Акушер-гинеколог</li>
								<li>Уролог</li>
								<li>Перинаталог</li>
							</ul>
						</div>
						<div class="more-info">
							<p>
								<strong>Общий медицинский стаж:</strong>
								<span>с 1996 ( 21 лет)</span>
							</p>
							<p>
								<strong>Ученая степень/категория:</strong>
								<span>Кандидат медицинских наук</span>
							</p>
						</div>
					</div>
				</div>
				<div class="cell-3 where">
					<p>Где ведет прием:</p>
					<a href="#">ВЕРАМЕД Одинцово, <br>г. Одинцово, ул. Говорова, 18/1</a>
					<a href="#">ВЕРАМЕД Премиум, <br>г. Одинцово, б-р Любы <br>Новоселовой, 17</a>
					<a href="#">ВЕРАМЕД Звенигород <br>г. Звенигород, ул. Московская, 12</a>
				</div>
			</div>
		</div>
	</div>
	<!-- / -->
	<div class="about-doctor">
		<div class="container">
			<h2 class="title">О враче</h2>
			<div class="grid">
				<div class="cell-10 shift-1">
					<div class="tabs">
						<ul>
							<li data-open="0" class="active">Образование</li>
							<li data-open="1">Достижения</li>
							<li data-open="2">Курсы</li>
							<li data-open="3">События</li>
							<li data-open="4">Статьи</li>
							<li data-open="5">Ведет прием</li>
							<li data-open="6">Заболевания</li>
						</ul>
					</div>
					<div class="hidden-blocks-slider" data-current-slide="0">
						<div class="item">
							<div class="list">
								<div class="element">
									<div>Образование</div>
									<div>Ивановская государственная медицинская академия имени А.С.Бубнова.</div>
								</div>
								<div class="element">
									<div>Специальность:</div>
									<div>«Лечебное дело».</div>
								</div>
								<div class="element">
									<div>Ординатура:</div>
									<div>1991-1992 гг. — интернатура по терапии на базе Центральной районной больницы города Иваново.</div>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="list">
								<div class="element">
									<div>Достижения</div>
									<div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis.</div>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="list">
								<div class="element">
									<div>Достижения</div>
									<div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis.</div>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="list">
								<div class="element">
									<div>Достижения</div>
									<div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis.</div>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="list">
								<div class="element">
									<div>Достижения</div>
									<div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis.</div>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="list">
								<div class="element">
									<div>Достижения</div>
									<div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis.</div>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="list">
								<div class="element">
									<div>Достижения</div>
									<div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis.</div>
								</div>
							</div>
						</div>								
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- / -->
	<div class="certificates">
		<h2 class="title">Сертификаты</h2>
		<div class="level-0">
			<div class="container">
				<div class="certificates-slider-one">
					<div class="item">
						<img src="/project/images/other/sert-1.jpg">
					</div>
					<div class="item">
						<img src="/project/images/other/vert-sert.jpeg">
					</div>
					<div class="item">
						<img src="/project/images/other/sert-1.jpg">
					</div>
					<div class="item">
						<img src="/project/images/other/vert-sert-2.jpeg">
					</div>
				</div>
			</div>
		</div>				
	</div>
	<!-- / -->
	<? include 'inc/modules/reviews.php';?>
	<!-- / -->
	<div class="price-block">
		<div class="container">
			<div class="grid">
				<div class="cell-10 shift-1">
					<div class="block-head">
						<h2 class="title">Прайс на услуги</h2>
						<p>Вы можете использовать поиск, для того, чтобы найти что-либо на нашем сайт, что вам очень пригодится. Мы рады вам помочь найти, что угодно, чтобы вы были рад.</p>
						<div class="tabs">
							<ul>
								<li class="active">Верамед Одинцово</li>
								<li>Верамед Премиум</li>
								<li>Верамед Звенигород</li>
							</ul>
						</div>
					</div>
					<div class="services">
						<div class="search">
							<input type="text" placeholder="Поиск услуг в прайсе">
						</div>
						<div class="list">
							<ul>
								<li>
									<div><span>Аллергология</span></div>
									<ul>
										<li><span>Вакуумное лигирование геморроидального узла латексным кольцом (1узел)</span><span>1400 Р</span></li>
										<li><span>Вскрытие абсцесса острого воспаления эпителиального копчикового хода</span><span>1400 Р</span></li>
										<li><span>Обработка анальной трещины (радиоволновым методом, лазером)</span><span>1400 Р</span></li>
										<li><span>Вскрытие абсцесса острого воспаления эпителиального копчикового хода</span><span>1400 Р</span></li>
										<li><span>Обработка анальной трещины (радиоволновым методом, лазером)</span><span>1400 Р</span></li>
										<li class="more">
											Показать еще 3 позиции
										</li>
									</ul>
								</li>
								<li>
									<div><span>Аппаратная коррекция функциональных расстройств ЦНС </span></div>
									<ul>
										<li><span>Вакуумное лигирование геморроидального узла латексным кольцом (1узел)</span><span>1400 Р</span></li>
										<li><span>Вскрытие абсцесса острого воспаления эпителиального копчикового хода</span><span>1400 Р</span></li>
										<li><span>Обработка анальной трещины (радиоволновым методом, лазером)</span><span>1400 Р</span></li>
										<li><span>Вскрытие абсцесса острого воспаления эпителиального копчикового хода</span><span>1400 Р</span></li>
										<li><span>Обработка анальной трещины (радиоволновым методом, лазером)</span><span>1400 Р</span></li>
										<li class="more">
											Показать еще 3 позиции
										</li>
									</ul>
								</li>
								<li>
									<div><span>Вакцинация</span></div>
									<ul>
										<li><span>Вакуумное лигирование геморроидального узла латексным кольцом (1узел)</span><span>1400 Р</span></li>
										<li><span>Вскрытие абсцесса острого воспаления эпителиального копчикового хода</span><span>1400 Р</span></li>
										<li><span>Обработка анальной трещины (радиоволновым методом, лазером)</span><span>1400 Р</span></li>
										<li><span>Вскрытие абсцесса острого воспаления эпителиального копчикового хода</span><span>1400 Р</span></li>
										<li><span>Обработка анальной трещины (радиоволновым методом, лазером)</span><span>1400 Р</span></li>
										<li class="more">
											Показать еще 3 позиции
										</li>
									</ul>
								</li>
								<li>
									<div>
										<span>Анализы</span>
										<span class="count">Всего услуг: <b>18</b></span>
									</div>
									<ul>
										<li>
											<span>Вакуумное лигирование геморроидального узла латексным кольцом (1узел)</span>
											<span>1400 Р</span>
										</li>
										<li><span>Вскрытие абсцесса острого воспаления эпителиального копчикового хода</span><span>1400 Р</span></li>
										<li><span>Обработка анальной трещины (радиоволновым методом, лазером)</span><span>1400 Р</span></li>
										<li><span>Вскрытие абсцесса острого воспаления эпителиального копчикового хода</span><span>1400 Р</span></li>
										<li><span>Обработка анальной трещины (радиоволновым методом, лазером)</span><span>1400 Р</span></li>
										<li class="more">
											Показать еще 3 позиции
										</li>
									</ul>
								</li>
								<li>
									<div><span>Гирудотерапия</span></div>
									<ul>
										<li><span>Вакуумное лигирование геморроидального узла латексным кольцом (1узел)</span><span>1400 Р</span></li>
										<li><span>Вскрытие абсцесса острого воспаления эпителиального копчикового хода</span><span>1400 Р</span></li>
										<li><span>Обработка анальной трещины (радиоволновым методом, лазером)</span><span>1400 Р</span></li>
										<li><span>Вскрытие абсцесса острого воспаления эпителиального копчикового хода</span><span>1400 Р</span></li>
										<li><span>Обработка анальной трещины (радиоволновым методом, лазером)</span><span>1400 Р</span></li>
										<li class="more">
											Показать еще 3 позиции
										</li>
									</ul>
								</li>
								<li>
									<div><span>Гомеопатия</span></div>
									<ul>
										<li><span>Вакуумное лигирование геморроидального узла латексным кольцом (1узел)</span><span>1400 Р</span></li>
										<li><span>Вскрытие абсцесса острого воспаления эпителиального копчикового хода</span><span>1400 Р</span></li>
										<li><span>Обработка анальной трещины (радиоволновым методом, лазером)</span><span>1400 Р</span></li>
										<li><span>Вскрытие абсцесса острого воспаления эпителиального копчикового хода</span><span>1400 Р</span></li>
										<li><span>Обработка анальной трещины (радиоволновым методом, лазером)</span><span>1400 Р</span></li>
										<li class="more">
											Показать еще 3 позиции
										</li>
									</ul>
								</li>
								<li>
									<div><span>Экзамен</span></div>
									<ul>
										<li><span>Вакуумное лигирование геморроидального узла латексным кольцом (1узел)</span><span>1400 Р</span></li>
										<li><span>Вскрытие абсцесса острого воспаления эпителиального копчикового хода</span><span>1400 Р</span></li>
										<li><span>Обработка анальной трещины (радиоволновым методом, лазером)</span><span>1400 Р</span></li>
										<li><span>Вскрытие абсцесса острого воспаления эпителиального копчикового хода</span><span>1400 Р</span></li>
										<li><span>Обработка анальной трещины (радиоволновым методом, лазером)</span><span>1400 Р</span></li>
										<li class="more">
											Показать еще 3 позиции
										</li>
									</ul>
								</li>
							</ul>
						</div>
						<div class="show-more">
							<div class="btn">Показать ещё 10 услуг</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- / -->
	<? include 'inc/modules/doctors-block.php';?>
	<!-- / -->
	<? include 'inc/modules/navigation.php';?>
	<!-- / -->
	<? include 'inc/modules/seo-block.php';?>
</div>