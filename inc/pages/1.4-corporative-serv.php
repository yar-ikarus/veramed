<div class="page-corporative-serv">
	<div class="corporative-serv-head">
		<? include 'inc/modules/breadcrumbs.php';?>
		<!-- / -->
		<div class="head">
			<div class="container">
				<h1 class="title">Оформление Мед.справок водителям</h1>
				<div class="grid">
					<div class="cell-10 shift-1">
						<div class="button">
							<span class="btn red2">Записаться</span>
						</div>
						<p>Платные медицинские услуги медцентра Верамед. Наш принцип — изо всех возможных методов лечения выбрать для пациента именно тот, который даст максимальную пользу.</p>
					</div>
				</div>
			</div>
		</div>
		<!-- / -->
		<div class="benefit">
			<div class="container">
				<h2 class="title">Преимущества</h2>
				<div class="grid">
					<div class="cell-4">
						<img src="/project/images/icons/benefit-white-1.png">
						<div>
							<span>Забота о здровье</span>
							<span>Воздухосодержание, как следует из раскручивает комплексный контр пример. </span>
						</div>
					</div>
					<div class="cell-4">
						<img src="/project/images/icons/benefit-white-2.png">
						<div>
							<span>Приветливый персонал</span>
							<span>Воздухосодержание, как следует из раскручивает комплексный контр пример. </span>
						</div>
					</div>
					<div class="cell-4">
						<img src="/project/images/icons/benefit-white-3.png">
						<div>
							<span>Профессионализм</span>
							<span>Воздухосодержание, как следует из раскручивает комплексный контр пример. </span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<? include 'inc/modules/timeline.php';?>
	
	<div class="simple-text">
		<div class="container">
			<h2 class="title">Подробнее о программе</h2>
			<div class="grid">
				<div class="cell-10 shift-1">
					<p>Лор - доктор, к которому обращаются родители с детьми, имеющими патологию верхних дыхательных путей: носа, горла, а также ушей. Такие заболевания составляют более 50% от всех случаев болезней у деток, поэтому детский лор - очень востребованный специалист. Доктор лечит, применяя терапевтические методы и в случае их неэффективности хирургическим путем, который на современном этапе является щадящим и малоинвазивным.</p>
					<p>Чаще всего причиной заболевания этих органов являются болезнетворные бактерии и вирусы, которые и вызывают риниты, фарингиты, отиты, ларингиты. Детский лор также занимается лечением новообразований и патологий, возникших вследствие врожденных анатомических аномалий лор органов. </p>
				</div>
			</div>
		</div>
	</div>
	<div class="quote">
		<div class="container">
			<div class="place">
				<p><strong>Детская поликлиника</strong> - общеизвестно, концентрирует из ряда вон выходящий ортогональный определитель. Используя таблицу интегралов элементарных функций, получим: медиабизнес уравновешивает инвестиционный продукт. Клиентский спрос, общеизвестно, традиционно искажает коллективный неопределенный интеграл.</p>
			</div>
		</div>
	</div>
	
	<!-- / -->
	<? include 'inc/modules/benefit.php';?>
	<!-- / -->
	<div class="price-type-2">
		<div class="container">
			<div class="grid">
				<div class="cell-8 shift-2">
					<h2 class="title">Прайс на услуги</h2>
					<div class="select">
						<select name="" id="">
							<option>Выберите клинику</option>
							<option value="">Выберите клинику</option>
							<option value="">Выберите клинику</option>
						</select>
					</div>
					<div class="list">
						<ul>
							<li><span>Вакуумное лигирование геморроидального узла латексным кольцом (1узел)</span><span>1400 Р</span></li>
							<li><span>Вскрытие абсцесса острого воспаления эпителиального копчикового хода</span><span>1400 Р</span></li>
							<li><span>Обработка анальной трещины (радиоволновым методом, лазером)</span><span>1400 Р</span></li>
							<li><span>Вскрытие абсцесса острого воспаления эпителиального копчикового хода</span><span>1400 Р</span></li>
							<li><span>Обработка анальной трещины (радиоволновым методом, лазером)</span><span>1400 Р</span></li>
						</ul>
					</div>
					<div class="show-more">
						<span class="btn">Раскрыть прайс-лист</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- / -->
	<? include 'inc/modules/navigation.php';?>
	<!-- / -->
	<? include 'inc/modules/seo-block.php';?>
</div>