<div class="page-search">
	<? include 'inc/modules/breadcrumbs.php';?>
	<!-- / -->
	<div class="head">
		<div class="container">
			<h1 class="title">Поиск</h1>
			<div class="grid">
				<div class="cell-10 shift-1">
					<p>Платные медицинские услуги медцентра Верамед. Наш принцип — изо всех возможных методов лечения выбрать для пациента именно тот, который даст максимальную пользу.</p>
				</div>
			</div>
		</div>
	</div>
	<!-- / -->
	<div class="search-place">
		<div class="container">
			<div class="grid">
				<div class="cell-10 shift-1">
					<div class="tabs">
						<ul>
							<li>Врачи</li>
							<li>Услуги</li>
							<li>Программы</li>
							<li>Акции</li>
						</ul>
					</div>
					<div class="search">
						<input type="text" placeholder="Поиск по сайту">
					</div>
					<div class="request">
						<div class="none">
							<p>По вашему запросу ничего нет</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>