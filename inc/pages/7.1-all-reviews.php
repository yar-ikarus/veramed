<div class="page-all-reviews">
	<? include 'inc/modules/breadcrumbs.php';?>
	<!-- / -->
	<div class="head">
		<div class="container">
			<h1 class="title">Оставить отзыв</h1>
			<div class="grid">
				<div class="cell-10 shift-1">
					<div class="button">
						<a href="#" class="btn red">Связаться со службой контроля качества</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- / -->
	<div class="reviews-list">
		<div class="container">
			<div class="grid">
				<div class="cell-8 shift-2">
					<div class="tabs">
						<ul>
							<li data-open="1" class="active">Отзывы про услуги</li>
							<li data-open="2">Отзывы врачей</li>
							<li data-open="3">Отзывы про бахллы</li>
						</ul>
					</div>
					<div class="hidden-blocks">
						<div class="block block-1">
							<div class="list">
								<?for ($i=0; $i < 2; $i++) { ?>
									<div class="item">
										<div class="group">
											<div class="cell">
												<div class="date">28 августа 2016, 16:42</div>
												<div class="tags">
													<ul>
														<li>Клиника в Одинцова</li>
														<li>Уролог</li>
														<li>Врач - Артюхов C.А.</li>
														<li>Урологические заболевания</li>
													</ul>
												</div>
												<div class="text">
													Уравнение малых колебаний относительно. Силовой трёхосный гироскопический стабилизатор стабилен. Точность крена позволяет исключить из рассмотрения лазерный ротор. Управление полётом самолёта активно...
												</div>
												<div class="show-more">
													<a href="#">Подробнее</a>
												</div>
											</div>
											<div class="cell">
												<div class="section">
													<div class="sec-title">
														Клиент:
													</div>
													<div class="name">
														<span><b>Коклюшевский</b> Константин Константинович</span>
													</div>
												</div>
												<div class="section client">
													<div class="sec-title">Отзыв врачу:</div>
													<div class="name">
														<div class="avatar" style="background-image: url(/project/images/other/avatar.jpg);"></div>
														<span><b>Сальникова</b> Ирина Александровна</span>
													</div>
												</div>
												<div class="all">
													<a href="#">Все отзывы</a>
												</div>
											</div>
										</div>
									</div>
								<?}?>
							</div>
							<div class="show-more">
								<div class="button">Показать больше</div>
							</div>
							<div class="paginator">
								<div class="to-start">
									<span>В начало</span>
								</div>
							</div>
						</div>
						<div class="block block-2">
							<div class="list">
								<?for ($i=0; $i < 2; $i++) { ?>
									<div class="item">
										<div class="group">
											<div class="cell">
												<div class="date">28 августа 2016, 16:42</div>
												<div class="tags">
													<ul>
														<li>Клиника в Одинцова</li>
														<li>Уролог</li>
														<li>Врач - Артюхов C.А.</li>
														<li>Урологические заболевания</li>
													</ul>
												</div>
												<div class="text">
													Уравнение малых колебаний относительно. Силовой трёхосный гироскопический стабилизатор стабилен. Точность крена позволяет исключить из рассмотрения лазерный ротор. Управление полётом самолёта активно...
												</div>
												<div class="show-more">
													<a href="#">Подробнее</a>
												</div>
											</div>
											<div class="cell">
												<div class="section">
													<div class="sec-title">
														Клиент:
													</div>
													<div class="name">
														<span><b>Коклюшевский</b> Константин Константинович</span>
													</div>
												</div>
												<div class="section client">
													<div class="sec-title">Отзыв врачу:</div>
													<div class="name">
														<div class="avatar" style="background-image: url(/project/images/other/avatar.jpg);"></div>
														<span><b>Сальникова</b> Ирина Александровна</span>
													</div>
												</div>
												<div class="all">
													<a href="#">Все отзывы</a>
												</div>
											</div>
										</div>
									</div>
								<?}?>
							</div>
							<div class="show-more">
								<div class="button">Показать больше</div>
							</div>
							<div class="paginator">
								<div class="to-start">
									<span>В начало</span>
								</div>
							</div>
						</div>
						<div class="block block-3">
							<div class="list">
								<?for ($i=0; $i < 2; $i++) { ?>
									<div class="item">
										<div class="group">
											<div class="cell">
												<div class="date">28 августа 2016, 16:42</div>
												<div class="tags">
													<ul>
														<li>Клиника в Одинцова</li>
														<li>Уролог</li>
														<li>Врач - Артюхов C.А.</li>
														<li>Урологические заболевания</li>
													</ul>
												</div>
												<div class="text">
													Уравнение малых колебаний относительно. Силовой трёхосный гироскопический стабилизатор стабилен. Точность крена позволяет исключить из рассмотрения лазерный ротор. Управление полётом самолёта активно...
												</div>
												<div class="show-more">
													<a href="#">Подробнее</a>
												</div>
											</div>
											<div class="cell">
												<div class="section">
													<div class="sec-title">
														Клиент:
													</div>
													<div class="name">
														<span><b>Коклюшевский</b> Константин Константинович</span>
													</div>
												</div>
												<div class="section client">
													<div class="sec-title">Отзыв врачу:</div>
													<div class="name">
														<div class="avatar" style="background-image: url(/project/images/other/avatar.jpg);"></div>
														<span><b>Сальникова</b> Ирина Александровна</span>
													</div>
												</div>
												<div class="all">
													<a href="#">Все отзывы</a>
												</div>
											</div>
										</div>
									</div>
								<?}?>
							</div>
							<div class="show-more">
								<div class="button">Показать больше</div>
							</div>
							<div class="paginator">
								<div class="to-start">
									<span>В начало</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- / -->
	<? include 'inc/modules/benefit.php';?>
	<!-- / -->
	<? include 'inc/modules/seo-block.php';?>
</div>