<div class="page-all-specs">
	<? include 'inc/modules/breadcrumbs.php';?>
	<div class="head">
		<div class="container">
			<h1 class="title">Врачи</h1>
			<div class="grid">
				<div class="cell-10 shift-1">
					<p>Платные медицинские услуги медцентра Верамед. Наш принцип — изо всех возможных методов лечения выбрать для пациента именно тот, который даст максимальную пользу.</p>
				</div>
			</div>
		</div>
	</div>
	
	<div class="doctors-big-list-block">
		<div class="container">
			<div class="filter">
				<div class="group">
					<div class="cell">
						<select name="">
							<option value="">ФИО Врача</option>
							<option value="">ФИО Врача</option>
							<option value="">ФИО Врача</option>
							<option value="">ФИО Врача</option>
						</select>
					</div>
					<div class="cell">
						<select name="">
							<option value="">Специализация</option>
							<option value="">Специализация</option>
							<option value="">Специализация</option>
							<option value="">Специализация</option>
						</select>
					</div>
					<div class="cell">
						<select name="">
							<option value="">Клиника</option>
							<option value="">Клиника</option>
							<option value="">Клиника</option>
							<option value="">Клиника</option>
						</select>
					</div>
					<div class="cell">
						<div class="switcher off"></div>
						<span>Ведет<br> прием детей</span>
					</div>
					<div class="cell">
						<div class="button"><span>Поиск</span></div>
					</div>
				</div>
			</div>
			<div class="big-list-block">
				<div class="alphabet">
					<div class="search">
						<input type="text" placeholder="Поиск услуги...">
					</div>
					<div class="place">
						<div class="letters">
							<ul>
								<li><span>А</span></li>
								<li><span>Б</span></li>
								<li><span>В</span></li>
								<li class="active"><span>Г</span></li>
								<li><span>Д</span></li>
								<li><span>И</span></li>
								<li><span>К</span></li>
								<li><span>Л</span></li>
								<li><span>М</span></li>
								<li><span>Н</span></li>
								<li><span>О</span></li>
								<li><span>П</span></li>
								<li><span>Р</span></li>
								<li><span>С</span></li>
								<li><span>Т</span></li>
								<li><span>У</span></li>
								<li><span>Ф</span></li>
								<li><span>Х</span></li>
								<li><span>Э</span></li>
							</ul>
						</div>
						<div class="list scrollbar-outer">
							<ul>
								<li>Акушер-гинеколог перинатолог</li>
								<li>Аллерголог-иммунолог</li>
								<li>Врач общей практики</li>
								<li>Гастроэнтеролог</li>
								<li>Гинеколог</li>
								<li>Гинеколог-эндокринолог</li>
								<li>Гирудотерапевт</li>
								<li>Гомеопат</li>
								<li>Дерматовенеролог</li>
								<li>Диетолог</li>
								<li>Иглорефлексотерапевт</li>
								<li>Кардиолог</li>
								<li>Колопроктолог</li>
								<li>Косметолог</li>
								<li>Логопед</li>
								<li>Маммолог</li>
								<li>Мануальный и Рефлексотерапевт</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="result-block">
					<div class="result-head">
						<h2>Акушер-Гинеколог, Перинатолог</h2>
						<div class="count"><span>32</span><span>Врача</span></div>
					</div>
					<div class="result-list">
						<? for ($i=0; $i < 9; $i++) { ?>
						<a class="item" href="#">
							<div class="avatar" style="background-image: url(/project/images/other/doctor-1.jpg);"></div>
							<div class="profession">Акушер-гинеколог, перинатолог</div>
							<div class="name">
								<b>Ахмедова</b>
								<span>Шамала</span>
								<span>Акаевна</span>
							</div>
							<div class="adress">
								<span>г. Одинцово,бул ... ( 3 )</span>
							</div>
						</a>
						<?}?>
					</div>
					<div class="show-more">
						<div class="button">Показать больше</div>
					</div>
					<div class="paginator">
						<div class="to-start">
							<span>В начало</span>
						</div>
						<div class="pag">
							<div class="prev"></div>
							<ul>
								<li><a href="#">1</a></li>
								<li><a href="#" class="current">2</a></li>
								<li><a href="#">3</a></li>
							</ul>
							<div class="next"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<? include 'inc/modules/reviews.php';?>
	<!-- // -->
	<? include 'inc/modules/benefit.php';?>
	
	<!-- // -->
	<? include 'inc/modules/seo-block.php';?>
</div>