<div class="page-all-clinic">
	<? include 'inc/modules/breadcrumbs.php';?>
	<!-- / -->
	<div class="head">
		<div class="container">
			<h1 class="title">Клиники</h1>
			<div class="grid">
				<div class="cell-10 shift-1">
					<p>Платные медицинские услуги медцентра Верамед. Наш принцип — изо всех возможных методов лечения выбрать для пациента именно тот, который даст максимальную пользу.</p>
				</div>
			</div>
		</div>
	</div>
	<!-- / -->
	<div class="place-card">
		<div class="container">
			<div class="grid">
				<div class="cell-3 image">
					<div class="img" style="background-image: url(/project/images/other/map-1.jpg);">
						<a href="#"></a>
					</div>
				</div>
				<div class="cell-6 info">
					<h2><a href="#">Верамед Одинцово</a></h2>
					<p>Медицинский центр «Верамед» ― это команда настоящих профессионалов, самое современное оборудование, эффективные методики и внимательный подход к каждому пациенту. </p>
					<p>Мы работаем в соответствии высоким международным стандартам и европейским требованиям к медицинскому обслуживанию. Вы можете быть уверены в качестве наших услуг!</p>
				</div>
				<div class="cell-3 contacts">
					<h2>Контакты</h2>
					<div class="location">
						<i class="icons-where-home"></i>
						<span>г. Одинцово, ул. Говорова, 18/1</span>
					</div>
					<div class="phone">
						<i class="icons-where-phone"></i>
						<span><a href="tel:8 (495) 150-34-86">8 (495) 150-34-86</a></span>
					</div>
					<div class="time">
						<i class="icons-where-time"></i>
						<div>
							<p><span class="d">Пн-сб:</span> <span class="t">08:00 - 20:00</span></p>
							<p><span class="d">Вс:</span> <span class="t">08:00 - 19:00</span></p>
						</div>
					</div>
					<div class="button">
						<a href="#" class="btn blue">Подробнее</a>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="grid">
				<div class="cell-3 image">
					<div class="img" style="background-image: url(/project/images/other/map-1.jpg);">
						<a href="#"></a>
					</div>
				</div>
				<div class="cell-6 info">
					<h2><a href="#">Верамед Одинцово</a></h2>
					<p>Медицинский центр «Верамед» ― это команда настоящих профессионалов, самое современное оборудование, эффективные методики и внимательный подход к каждому пациенту. </p>
					<p>Мы работаем в соответствии высоким международным стандартам и европейским требованиям к медицинскому обслуживанию. Вы можете быть уверены в качестве наших услуг!</p>
				</div>
				<div class="cell-3 contacts">
					<h2>Контакты</h2>
					<div class="location">
						<i class="icons-where-home"></i>
						<span>г. Одинцово, ул. Говорова, 18/1</span>
					</div>
					<div class="phone">
						<i class="icons-where-phone"></i>
						<span><a href="tel:8 (495) 150-34-86">8 (495) 150-34-86</a></span>
					</div>
					<div class="time">
						<i class="icons-where-time"></i>
						<div>
							<p><span class="d">Пн-сб:</span> <span class="t">08:00 - 20:00</span></p>
							<p><span class="d">Вс:</span> <span class="t">08:00 - 19:00</span></p>
						</div>
					</div>
					<div class="button">
						<a href="#" class="btn blue">Подробнее</a>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="grid">
				<div class="cell-3 image">
					<div class="img" style="background-image: url(/project/images/other/map-1.jpg);"><a href="#"></a></div>
				</div>
				<div class="cell-6 info">
					<h2><a href="#">Верамед Одинцово</a></h2>
					<p>Медицинский центр «Верамед» ― это команда настоящих профессионалов, самое современное оборудование, эффективные методики и внимательный подход к каждому пациенту. </p>
					<p>Мы работаем в соответствии высоким международным стандартам и европейским требованиям к медицинскому обслуживанию. Вы можете быть уверены в качестве наших услуг!</p>
				</div>
				<div class="cell-3 contacts">
					<h2>Контакты</h2>
					<div class="location">
						<i class="icons-where-home"></i>
						<span>г. Одинцово, ул. Говорова, 18/1</span>
					</div>
					<div class="phone">
						<i class="icons-where-phone"></i>
						<span><a href="tel:8 (495) 150-34-86">8 (495) 150-34-86</a></span>
					</div>
					<div class="time">
						<i class="icons-where-time"></i>
						<div>
							<p><span class="d">Пн-сб:</span> <span class="t">08:00 - 20:00</span></p>
							<p><span class="d">Вс:</span> <span class="t">08:00 - 19:00</span></p>
						</div>
					</div>
					<div class="button">
						<a href="#" class="btn blue">Подробнее</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- / -->
	
	<div class="container">
		<h2 class="title">SEO блок с описанием</h2>
	</div>
	<div class="map-place">
		<div class="container">
			<div class="place">
				<div class="search">
					<div class="search-place">
						<input type="text" placeholder="Найдите клинику (введите свой домашний или рабочий адрес)">
					</div>
				</div>
				<div class="map">
					<!-- это заглушка -->
					<div class="demo" style="background-image: url(/project/images/other/map-demo.jpg); height: 250px; width: 100%; -webkit-background-size: cover;
					background-size: cover;"></div>
				</div>
			</div>
		</div>
	</div>

	<!-- / -->

	<? include 'inc/modules/seo-block.php';?>
	<!-- / -->
</div>