<div class="page-one-review">
	<? include 'inc/modules/breadcrumbs.php';?>
	<!-- / -->
	<div class="head">
		<div class="container">
			<h1 class="title">Отзыв о Верамед Звенигород</h1>
			<div class="grid">
				<div class="cell-10 shift-1">
					<p>Платные медицинские услуги медцентра Верамед. Наш принцип — изо всех возможных методов лечения выбрать для пациента именно тот, который даст максимальную пользу.</p>
				</div>
			</div>
		</div>
	</div>
	<!-- / -->
	<div class="place-card">
		<div class="container">
			<div class="grid">
				<div class="cell-3 image">
					<div class="img" style="background-image: url(/project/images/other/map-1.jpg);"></div>
				</div>
				<div class="cell-6 info">
					<h2>Верамед Одинцово</h2>
					<p>Медицинский центр «Верамед» ― это команда настоящих профессионалов, самое современное оборудование, эффективные методики и внимательный подход к каждому пациенту. </p>
					<p>Мы работаем в соответствии высоким международным стандартам и европейским требованиям к медицинскому обслуживанию. Вы можете быть уверены в качестве наших услуг!</p>
				</div>
				<div class="cell-3 contacts">
					<h2>Контакты</h2>
					<div class="location">
						<i class="icons-where-home"></i>
						<span>г. Одинцово, ул. Говорова, 18/1</span>
					</div>
					<div class="phone">
						<i class="icons-where-phone"></i>
						<span>8 (495) 150-34-86</span>
					</div>
					<div class="time">
						<i class="icons-where-time"></i>
						<div>
							<p><span class="d">Пн-сб:</span> <span class="t">08:00 - 20:00</span></p>
							<p><span class="d">Вс:</span> <span class="t">08:00 - 19:00</span></p>
						</div>
					</div>
					<div class="button">
						<div class="btn blue get-callback">Связаться со мной</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- / -->
	<div class="reviews-list">
		<div class="container">
			<div class="grid">
				<div class="cell-8 shift-2">
					<h2 class="title">Последние отзывы</h2>
					<div class="list">
						<?for ($i=0; $i < 3; $i++) { ?>
							<div class="item">
								<div class="group">
									<div class="cell">
										<div class="date">28 августа 2016, 16:42</div>
										<div class="tags">
											<ul>
												<li>Клиника в Одинцова</li>
												<li>Уролог</li>
												<li>Врач - Артюхов C.А.</li>
												<li>Урологические заболевания</li>
											</ul>
										</div>
										<div class="text">
											Уравнение малых колебаний относительно. Силовой трёхосный гироскопический стабилизатор стабилен. Точность крена позволяет исключить из рассмотрения лазерный ротор. Управление полётом самолёта активно...
										</div>
										<div class="show-more">
											<a href="#">Подробнее</a>
										</div>
									</div>
									<div class="cell">
										<div class="section">
											<div class="sec-title">
												Клиент:
											</div>
											<div class="name">
												<span><b>Коклюшевский</b> Константин Константинович</span>
											</div>
										</div>
										<div class="section client">
											<div class="sec-title">Отзыв врачу:</div>
											<div class="name">
												<div class="avatar" style="background-image: url(/project/images/other/avatar.jpg);"></div>
												<span><b>Сальникова</b> Ирина Александровна</span>
											</div>
										</div>
										<div class="all">
											<a href="#">Все отзывы</a>
										</div>
									</div>
								</div>
							</div>
						<?}?>
					</div>
					<div class="show-more">
						<div class="button">Показать больше</div>
					</div>
					<div class="paginator">
						<div class="to-start">
							<span>В начало</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- / -->
	<? include 'inc/modules/navigation.php';?>
	<!-- / -->
	<? include 'inc/modules/seo-block.php';?>
</div>