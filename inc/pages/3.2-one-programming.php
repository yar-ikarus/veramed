<div class="page-one-programming">
	<div class="one-programming-head">
		<? include 'inc/modules/breadcrumbs.php';?>
		<!-- / -->
		<div class="head">
			<div class="container">
				<h1 class="title">Курс ФизиоТерапевтической реабилитации</h1>
				<div class="grid">
					<div class="cell-10 shift-1">
						<div class="group">
							<div class="cell price">
								<span>9 990 Р</span>
							</div>
							<div class="cell">
								<select name="">
									<option>Выбрать клинику</option>
									<option value="">Выбрать клинику</option>
									<option value="">Выбрать клинику</option>
									<option value="">Выбрать клинику</option>
								</select>
							</div>
							<div class="cell">
								<div class="button">
									<span class="btn blue">Записаться</span>
								</div>
							</div>
						</div>
						<p>Платные медицинские услуги медцентра Верамед. Наш принцип — изо всех возможных методов лечения выбрать для пациента именно тот, который даст максимальную пользу.</p>
					</div>
				</div>
			</div>
		</div>
		<!-- / -->
		<div class="benefit">
			<div class="container">
				<h2 class="title">Преимущества</h2>
				<div class="grid">
					<div class="cell-4">
						<img src="/project/images/icons/benefit-white-1.png">
						<div>
							<span>Забота о здровье</span>
							<span>Воздухосодержание, как следует из раскручивает комплексный контр пример. </span>
						</div>
					</div>
					<div class="cell-4">
						<img src="/project/images/icons/benefit-white-2.png">
						<div>
							<span>Приветливый персонал</span>
							<span>Воздухосодержание, как следует из раскручивает комплексный контр пример. </span>
						</div>
					</div>
					<div class="cell-4">
						<img src="/project/images/icons/benefit-white-3.png">
						<div>
							<span>Профессионализм</span>
							<span>Воздухосодержание, как следует из раскручивает комплексный контр пример. </span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="price-block type-two">
		<div class="container">
			<div class="grid">
				<div class="cell-10 shift-1">
					<div class="block-head">
						<h2 class="title">Прайс на услуги</h2>
						<p>Вы можете использовать поиск, для того, чтобы найти что-либо на нашем сайт, что вам очень пригодится. Мы рады вам помочь найти, что угодно, чтобы вы были рад.</p>
						<div class="tabs">
							<ul>
								<li class="active">Верамед Одинцово</li>
								<li>Верамед Премиум</li>
								<li>Верамед Звенигород</li>
							</ul>
						</div>
					</div>
					<div class="services">
						<div class="list">
							<ul>
								<li>
									<div><span>Аллергология</span></div>
								</li>
								<li>
									<div><span>Аппаратная коррекция функциональных расстройств ЦНС </span></div>
								</li>
								<li>
									<div><span>Вакцинация</span></div>
								</li>
								<li>
									<div>
										<span>Анализы</span>
										<span class="count">Всего услуг: <b>18</b></span>
									</div>
								</li>
								<li>
									<div><span>Гирудотерапия</span></div>
								</li>
								<li>
									<div><span>Гомеопатия</span></div>
								</li>
								<li>
									<div><span>Экзамен</span></div>
								</li>
							</ul>
						</div>
						<div class="show-more">
							<div class="btn">Показать все услуги</div>
						</div>
						<div class="final-price">
							<span>Стоимость программы:</span>
							<span>28 999 Р</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="info-text-block">
		<div class="container">
			<div class="grid">
				<div class="cell-10 shift-1">
					<h2 class="title">SEO блок с описанием</h2>
					<div class="tabs">
						<ul>
							<li data-open="1" class="active">Наблюдение</li>
							<li data-open="2">Спектр услуг</li>
							<li data-open="3">Качество</li>
						</ul>
					</div>
					<div class="text-block block-1">
						<p>Таргетирование конструктивно. Абстракционизм, пренебрегая деталями, основательно подпорчен предыдущим опытом применения. Олицетворение, вопреки мнению П.Друкера, перпендикулярно. Очевидно, что анжамбеман текстологически осознаёт глубокий медиаплан. Наряду с нейтральной лексикой баннерная реклама спорадически программирует хорей.</p>
						<p>Поведенческий таргетинг, вопреки мнению П.Друкера, дает конкурент. После того как тема сформулирована, сервисная стратегия порождена временем. Согласно последним исследованиям, осведомленность о бренде выбирает литературный фирменный стиль. Образ аллитерирует конвергентный парафраз. Рекламное сообщество, без использования формальных признаков поэзии, оправдывает дольник.</p>
					</div>
					<div class="text-block block-2">
						<p>Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот, несмотря на название, не имеет никакого отношения к обитателям водоемов. Используется он веб-дизайнерами для вставки на интернет-страницы и демонстрации внешнего вида контента, просмотра шрифтов, абзацев, отступов и т.д. Так как цель применения такого текста исключительно демонстрационная, то и смысловую нагрузку ему нести совсем необязательно. Более того, нечитабельность текста сыграет на руку при оценке качества восприятия макета.</p>
					</div>
					<div class="text-block block-3">
						<p>Каждый веб-разработчик знает, что такое текст-«рыба». Текст этот, несмотря на название, не имеет никакого отношения к обитателям водоемов. Используется он веб-дизайнерами для вставки на интернет-страницы и демонстрации внешнего вида контента, просмотра шрифтов, абзацев, отступов и т.д. Так как цель применения такого текста исключительно демонстрационная, то и смысловую нагрузку ему нести совсем необязательно. Более того, нечитабельность текста сыграет на руку при оценке качества восприятия макета.</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<? include 'inc/modules/doctors-block.php';?>
	<!-- / -->
	<? include 'inc/modules/reviews.php';?>
	<!-- / -->
	<? include 'inc/modules/where-block.php';?>
	<!-- / -->
	<div class="new-programs">
		<div class="container">
			<h2 class="title">Новые программы</h2>
			<div class="slider-new-programs">
				<? for ($i=0; $i < 4; $i++) { ?>
				<div class="item">
					<div class="img" style="background-image: url(/project/images/other/programm_1.jpg);">
						<a href="#"></a>
					</div>
					<div class="place">
						<div class="level-0">Программа</div>
						<div class="level-1">
							<div><a href="#">Реабилитационные физиотерапевтические комплексы</a></div>
							<div>
								<span>25.000 - <br>50.000 Р</span>
							</div>
						</div>
						<div class="level-2">
							<ul>
								<li><span>Только качественные методы лечения</span></li>
								<li><span>Профессиональные врачи и персонал клиники</span></li>
							</ul>
						</div>
						<div class="level-3">
							<div class="more"><a href="#">Подробнее</a></div>
							<div class="tags">
								<div class="tag">
									<img src="/project/images/icons/prog-tag-1.png">
									<span>Для женщин</span>
								</div>
								<div class="tag">
									<img src="/project/images/icons/prog-tag-2.png">
									<span>Горячие новинки</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?}?>
			</div>
		</div>
	</div>
	<!-- / -->
	<? include 'inc/modules/navigation.php';?>
	<!-- / -->
</div>