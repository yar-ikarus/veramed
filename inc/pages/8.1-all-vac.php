<div class="page-all-vac">
	<? include 'inc/modules/breadcrumbs.php';?>
	<!-- / -->
	<div class="head">
		<div class="container">
			<h1 class="title">Вакансии</h1>
			<div class="grid">
				<div class="cell-10 shift-1">
					<p>Платные медицинские услуги медцентра Верамед. Наш принцип — изо всех возможных методов лечения выбрать для пациента именно тот, который даст максимальную пользу.</p>
				</div>
			</div>
		</div>
	</div>
	<!-- / -->
	
	<!-- / -->

	<div class="vac-list">
		<div class="container">
			<div class="grid">
				<div class="cell-8 shift-2">
					<div class="tabs">
						<ul>
							<li data-open="1" class="active">Все вакансии</li>
							<li data-open="2">Врачи</li>
							<li data-open="3">Медсестры</li>
							<li data-open="4">Администрация</li>
						</ul>
					</div>
					<div class="hidden-blocks">
						<div class="block block-1">
							<div class="list">
								<div class="item">
									<div class="level-0">
										<span>28 августа 2016, 16:42</span>
									</div>
									<div class="level-1">
										<div class="name"><a href="#">Менеджер по продажам, менеджер по работе с клиентами</a></div>
										<div class="price">
											<span class="fix">25.000 - 50.000 Р</span>
										</div>
									</div>
									<div class="level-2">
										<div class="tags">
											<ul>
												<li>Акушер-гинеколог</li>
												<li>Уролог</li>
												<li>Перинаталог</li>
											</ul>
										</div>
									</div>
									<div class="level-3">
										<i class="icons-point"></i>
										<span><b>ВЕРАМЕД Одинцово</b> г. Одинцово, бул. Любы Новоселовой, 17</span>
									</div>
									<div class="level-4">
										<a href="#">Подробнее</a>
									</div>
								</div>
								<div class="item">
									<div class="level-0">
										<span>28 августа 2016, 16:42</span>
									</div>
									<div class="level-1">
										<div class="name"><a href="#">Менеджер по продажам, менеджер по работе с клиентами</a></div>
										<div class="price">
											<span class="agreement">По договоренности</span>
										</div>
									</div>
									<div class="level-2">
										<div class="tags">
											<ul>
												<li>Акушер-гинеколог</li>
												<li>Уролог</li>
												<li>Перинаталог</li>
											</ul>
										</div>
									</div>
									<div class="level-3">
										<i class="icons-point"></i>
										<span><b>ВЕРАМЕД Одинцово</b> г. Одинцово, бул. Любы Новоселовой, 17</span>
									</div>
									<div class="level-4">
										<a href="#">Подробнее</a>
									</div>
								</div>
								<div class="item">
									<div class="level-0">
										<span>28 августа 2016, 16:42</span>
									</div>
									<div class="level-1">
										<div class="name"><a href="#">Менеджер по продажам, менеджер по работе с клиентами</a></div>
										<div class="price">
											<span class="fix">25.000 - 50.000 Р</span>
										</div>
									</div>
									<div class="level-2">
										<div class="tags">
											<ul>
												<li>Акушер-гинеколог</li>
												<li>Уролог</li>
												<li>Перинаталог</li>
											</ul>
										</div>
									</div>
									<div class="level-3">
										<i class="icons-point"></i>
										<span><b>ВЕРАМЕД Одинцово</b> г. Одинцово, бул. Любы Новоселовой, 17</span>
									</div>
									<div class="level-4">
										<a href="#">Подробнее</a>
									</div>
								</div>
							</div>
						</div>
						<div class="block block-2">
							<div class="list">
								<div class="item">
									<div class="level-0">
										<span>28 августа 2016, 16:42</span>
									</div>
									<div class="level-1">
										<div class="name"><a href="#">Менеджер по продажам, менеджер по работе с клиентами</a></div>
										<div class="price">
											<span class="fix">25.000 - 50.000 Р</span>
										</div>
									</div>
									<div class="level-2">
										<div class="tags">
											<ul>
												<li>Акушер-гинеколог</li>
												<li>Уролог</li>
												<li>Перинаталог</li>
											</ul>
										</div>
									</div>
									<div class="level-3">
										<i class="icons-point"></i>
										<span><b>ВЕРАМЕД Одинцово</b> г. Одинцово, бул. Любы Новоселовой, 17</span>
									</div>
									<div class="level-4">
										<a href="#">Подробнее</a>
									</div>
								</div>
								<div class="item">
									<div class="level-0">
										<span>28 августа 2016, 16:42</span>
									</div>
									<div class="level-1">
										<div class="name"><a href="#">Менеджер по продажам, менеджер по работе с клиентами</a></div>
										<div class="price">
											<span class="agreement">По договоренности</span>
										</div>
									</div>
									<div class="level-2">
										<div class="tags">
											<ul>
												<li>Акушер-гинеколог</li>
												<li>Уролог</li>
												<li>Перинаталог</li>
											</ul>
										</div>
									</div>
									<div class="level-3">
										<i class="icons-point"></i>
										<span><b>ВЕРАМЕД Одинцово</b> г. Одинцово, бул. Любы Новоселовой, 17</span>
									</div>
									<div class="level-4">
										<a href="#">Подробнее</a>
									</div>
								</div>
								<div class="item">
									<div class="level-0">
										<span>28 августа 2016, 16:42</span>
									</div>
									<div class="level-1">
										<div class="name"><a href="#">Менеджер по продажам, менеджер по работе с клиентами</a></div>
										<div class="price">
											<span class="fix">25.000 - 50.000 Р</span>
										</div>
									</div>
									<div class="level-2">
										<div class="tags">
											<ul>
												<li>Акушер-гинеколог</li>
												<li>Уролог</li>
												<li>Перинаталог</li>
											</ul>
										</div>
									</div>
									<div class="level-3">
										<i class="icons-point"></i>
										<span><b>ВЕРАМЕД Одинцово</b> г. Одинцово, бул. Любы Новоселовой, 17</span>
									</div>
									<div class="level-4">
										<a href="#">Подробнее</a>
									</div>
								</div>
							</div>
						</div>
						<div class="block block-3">
							<div class="list">
								<div class="item">
									<div class="level-0">
										<span>28 августа 2016, 16:42</span>
									</div>
									<div class="level-1">
										<div class="name"><a href="#">Менеджер по продажам, менеджер по работе с клиентами</a></div>
										<div class="price">
											<span class="fix">25.000 - 50.000 Р</span>
										</div>
									</div>
									<div class="level-2">
										<div class="tags">
											<ul>
												<li>Акушер-гинеколог</li>
												<li>Уролог</li>
												<li>Перинаталог</li>
											</ul>
										</div>
									</div>
									<div class="level-3">
										<i class="icons-point"></i>
										<span><b>ВЕРАМЕД Одинцово</b> г. Одинцово, бул. Любы Новоселовой, 17</span>
									</div>
									<div class="level-4">
										<a href="#">Подробнее</a>
									</div>
								</div>
								<div class="item">
									<div class="level-0">
										<span>28 августа 2016, 16:42</span>
									</div>
									<div class="level-1">
										<div class="name"><a href="#">Менеджер по продажам, менеджер по работе с клиентами</a></div>
										<div class="price">
											<span class="agreement">По договоренности</span>
										</div>
									</div>
									<div class="level-2">
										<div class="tags">
											<ul>
												<li>Акушер-гинеколог</li>
												<li>Уролог</li>
												<li>Перинаталог</li>
											</ul>
										</div>
									</div>
									<div class="level-3">
										<i class="icons-point"></i>
										<span><b>ВЕРАМЕД Одинцово</b> г. Одинцово, бул. Любы Новоселовой, 17</span>
									</div>
									<div class="level-4">
										<a href="#">Подробнее</a>
									</div>
								</div>
								<div class="item">
									<div class="level-0">
										<span>28 августа 2016, 16:42</span>
									</div>
									<div class="level-1">
										<div class="name"><a href="#">Менеджер по продажам, менеджер по работе с клиентами</a></div>
										<div class="price">
											<span class="fix">25.000 - 50.000 Р</span>
										</div>
									</div>
									<div class="level-2">
										<div class="tags">
											<ul>
												<li>Акушер-гинеколог</li>
												<li>Уролог</li>
												<li>Перинаталог</li>
											</ul>
										</div>
									</div>
									<div class="level-3">
										<i class="icons-point"></i>
										<span><b>ВЕРАМЕД Одинцово</b> г. Одинцово, бул. Любы Новоселовой, 17</span>
									</div>
									<div class="level-4">
										<a href="#">Подробнее</a>
									</div>
								</div>
							</div>
						</div>
						<div class="block block-4">
							<div class="list">
								<div class="item">
									<div class="level-0">
										<span>28 августа 2016, 16:42</span>
									</div>
									<div class="level-1">
										<div class="name"><a href="#">Менеджер по продажам, менеджер по работе с клиентами</a></div>
										<div class="price">
											<span class="fix">25.000 - 50.000 Р</span>
										</div>
									</div>
									<div class="level-2">
										<div class="tags">
											<ul>
												<li>Акушер-гинеколог</li>
												<li>Уролог</li>
												<li>Перинаталог</li>
											</ul>
										</div>
									</div>
									<div class="level-3">
										<i class="icons-point"></i>
										<span><b>ВЕРАМЕД Одинцово</b> г. Одинцово, бул. Любы Новоселовой, 17</span>
									</div>
									<div class="level-4">
										<a href="#">Подробнее</a>
									</div>
								</div>
								<div class="item">
									<div class="level-0">
										<span>28 августа 2016, 16:42</span>
									</div>
									<div class="level-1">
										<div class="name"><a href="#">Менеджер по продажам, менеджер по работе с клиентами</a></div>
										<div class="price">
											<span class="agreement">По договоренности</span>
										</div>
									</div>
									<div class="level-2">
										<div class="tags">
											<ul>
												<li>Акушер-гинеколог</li>
												<li>Уролог</li>
												<li>Перинаталог</li>
											</ul>
										</div>
									</div>
									<div class="level-3">
										<i class="icons-point"></i>
										<span><b>ВЕРАМЕД Одинцово</b> г. Одинцово, бул. Любы Новоселовой, 17</span>
									</div>
									<div class="level-4">
										<a href="#">Подробнее</a>
									</div>
								</div>
								<div class="item">
									<div class="level-0">
										<span>28 августа 2016, 16:42</span>
									</div>
									<div class="level-1">
										<div class="name"><a href="#">Менеджер по продажам, менеджер по работе с клиентами</a></div>
										<div class="price">
											<span class="fix">25.000 - 50.000 Р</span>
										</div>
									</div>
									<div class="level-2">
										<div class="tags">
											<ul>
												<li>Акушер-гинеколог</li>
												<li>Уролог</li>
												<li>Перинаталог</li>
											</ul>
										</div>
									</div>
									<div class="level-3">
										<i class="icons-point"></i>
										<span><b>ВЕРАМЕД Одинцово</b> г. Одинцово, бул. Любы Новоселовой, 17</span>
									</div>
									<div class="level-4">
										<a href="#">Подробнее</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="show-more">
						<div class="button">Показать больше</div>
					</div>
					<div class="paginator">
						<div class="to-start">
							<span>В начало</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- / -->
	<? include 'inc/modules/benefit.php';?>
	<!-- / -->
	<? include 'inc/modules/navigation.php';?>
	<!-- / -->
	<? include 'inc/modules/seo-block.php';?>
</div>