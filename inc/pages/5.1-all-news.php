<div class="page-all-news">
	<? include 'inc/modules/breadcrumbs.php';?>
	<!-- / -->
	<div class="head">
		<div class="container">
			<div class="grid">
				<div class="cell-8 shift-2">
					<h1 class="title">Новости</h1>
					<p>Платные медицинские услуги медцентра Верамед. Наш принцип — изо всех возможных методов лечения выбрать для пациента именно тот, который даст максимальную пользу.</p>
				</div>
			</div>
		</div>
	</div>
	<!-- / -->
	
	<div class="all-news-list">
		<div class="container">
			
			<div class="list">
				<? for ($i=0; $i < 6; $i++) { ?>
				<div class="item">
					<div class="place">
						<div class="level-0">1 сентября 2016, 08:15</div>
						<div class="level-1"><a href="#">Эпидсезон по вирусному энцефалиту начался в России</a></div>
						<div class="level-2">С 1 апреля Роспотребнадзор начнет отслеживать...</div>
						<div class="level-3"><a href="#">Подробнее</a></div>
					</div>
				</div>
				<div class="item">
					<div class="place">
						<div class="level-0">1 сентября 2016, 08:15</div>
						<div class="level-1"><a href="#">С Днем медицинского работника!</a></div>
						<div class="level-2">Красоту женских ног всегда воспевали поэты...</div>
						<div class="level-3"><a href="#">Подробнее</a></div>
					</div>
				</div>
				<?}?>
			</div>
			<div class="show-more">
				<div class="button">Показать больше</div>
			</div>
			<div class="paginator">
				<div class="to-start">
					<span>В начало</span>
				</div>
			</div>
		</div>
	</div>

	<!-- / -->
	<? include 'inc/modules/articles.php';?>
	
	<!-- / -->

	<? include 'inc/modules/benefit.php';?>
	<!-- / -->
	<? include 'inc/modules/navigation.php';?>
</div>