<div class="page-main">
	

	<div class="index-slider-place">
		<div class="container">
			<div class="index-slider">
				<div class="item">
					<div class="slide slide-1">
						<img src="/project/images/other/slide-1.png" class="img">
						<div class="text">Прокачай иммунитет всего за 10 дней!</div>
						<div class="more"><a href="#" class="btn">Подробнее</a></div>
					</div>
				</div>
				<div class="item">
					<div class="slide slide-2">
						<img src="/project/images/other/slide-2.png" class="img">
						<div class="text">
							Не будь беспечен, проверь печень!
							<br>
							<img src="/project/images/other/pechen.png" style="margin-top: 6px;">
						</div>
						<div class="more"><a href="#" class="btn">Подробнее</a></div>
					</div>
				</div>
				<div class="item">
					<div class="slide slide-3">
						<img src="/project/images/other/slide-3.png" class="img">
						<div class="text">В день лечения консультация стоматолога бесплатно!</div>
						<div class="more"><a href="#" class="btn">Подробнее</a></div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<? include 'inc/modules/benefit.php';?>
	

	<div class="specialists">
		<div class="container">
			<h2 class="title">Наши специалисты</h2>
			<div class="filter">
				<span>Фильтр:</span>
				<ul>
					<li><a href="#">ВСЕ</a></li>
					<li><a href="#">А</a></li>
					<li><a href="#">Б</a></li>
					<li><a href="#">В</a></li>
					<li><a href="#">Г</a></li>
					<li><a href="#">Д</a></li>
					<li><a href="#">Е</a></li>
					<li><a href="#">Ж</a></li>
					<li><a href="#">З</a></li>
					<li><a href="#">И</a></li>
					<li><a href="#">Й</a></li>
					<li><a href="#">К</a></li>
					<li><a href="#">Л</a></li>
					<li><a href="#">М</a></li>
					<li><a href="#">Н</a></li>
					<li><a href="#">О</a></li>
					<li><a href="#">П</a></li>
					<li><a href="#">Р</a></li>
					<li><a href="#">С</a></li>
					<li><a href="#">Т</a></li>
					<li><a href="#">У</a></li>
					<li><a href="#">Ф</a></li>
					<li><a href="#">Х</a></li>
					<li><a href="#">Ц</a></li>
					<li><a href="#">Ч</a></li>
					<li><a href="#">Ш</a></li>
					<li><a href="#">Щ</a></li>
					<li><a href="#">Э</a></li>
					<li><a href="#">Ю</a></li>
					<li><a href="#">Я</a></li>
				</ul>
			</div>
			<div class="result">
				<ul>
					<li class="first"><a href="#">Акушер-гинеколог, перинатолог </a></li>
					<li><a href="#">Аллерголог-иммунолог</a></li>
					<li class="first"><a href="#">Врач общей практики</a></li>
					<li class="first"><a href="#">Гастроэнтерфолог</a></li>
					<li><a href="#">Гинеколог</a></li>
					<li><a href="#">Гинеколог-эндокринолог</a></li>
					<li><a href="#">Гирудотерапевт</a></li>
					<li><a href="#">Гомеопат</a></li>
					<li class="first"><a href="#">Дерматовенеролог</a></li>
					<li><a href="#">Диетолог</a></li>
					<li class="first"><a href="#">Иглорефлексотерапевт</a></li>
				</ul>
				<ul>
					<li class="first"><a href="#">Колопроктолог</a></li>
					<li><a href="#">Косметолог</a></li>
					<li class="first"><a href="#">Логопед</a></li>
					<li class="first"><a href="#">Маммолог</a></li>
					<li><a href="#">Мануальный и Рефлексотерапевт</a></li>
					<li><a href="#">Массажист</a></li>
					<li><a href="#">Медицинская сестра</a></li>
					<li class="first"><a href="#">Нарколог</a></li>
					<li><a href="#">Невролог</a></li>
					<li><a href="#">Нейропсихолог</a></li>
					<li><a href="#">Нейрохирург</a></li>
				</ul>
				<ul>
					<li><a href="#">Нефролог</a></li>
					<li class="first"><a href="#">Озонотерапевт</a></li>
					<li><a href="#">Онколог</a></li>
					<li><a href="#">Ортопед</a></li>
					<li><a href="#">Оториноларинголог </a></li>
					<li><a href="#">Офтальмолог</a></li>
					<li><a href="#">Офтальмохирург</a></li>
					<li class="first"><a href="#">Педиатр</a></li>
					<li><a href="#">Психиатр</a></li>
					<li><a href="#">Психолог</a></li>
					<li><a href="#">Психотерапевт</a></li>
				</ul>
				<ul>
					<li class="first"><a href="#">Ревматолог</a></li>
					<li class="first"><a href="#">Стоматолог</a></li>
					<li class="first"><a href="#">Терапевт</a></li>
					<li><a href="#">Травматолог</a></li>
					<li class="first"><a href="#">Уролог-андролог</a></li>
					<li class="first"><a href="#">Физиотерапевт</a></li>
					<li><a href="#">Флеболог</a></li>
					<li><a href="#">Фтизиатр</a></li>
					<li><a href="#">Функциональная диагностика (УЗИ)</a></li>
					<li class="first"><a href="#">Хирург</a></li>
					<li class="first"><a href="#">Эндокринолог</a></li>
					<li><a href="#">Эндоскопист</a></li>
				</ul>
			</div>
			<div class="more">
				<a href="#" class="btn">Показать больше</a>
			</div>
		</div>
	</div>
	
	<? include 'inc/modules/news-block.php';?>

	<? include 'inc/modules/reviews.php';?>
	<!-- / -->

	<? include 'inc/modules/seo-block.php';?>
	<!-- / -->
</div>