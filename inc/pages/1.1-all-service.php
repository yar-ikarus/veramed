<div class="page-all-specs">
	<? include 'inc/modules/breadcrumbs.php';?>
	<div class="head">
		<div class="container">
			<h1 class="title">Услуги</h1>
			<div class="grid">
				<div class="cell-10 shift-1">
					<p>Платные медицинские услуги медцентра Верамед. Наш принцип — изо всех возможных методов лечения выбрать для пациента именно тот, который даст максимальную пользу.</p>
				</div>
			</div>
		</div>
	</div>
	
	<div class="doctors-big-list-block">
		<div class="container">
			<div class="filter">
				<div class="group">
					<div class="cell">
						<select name="">
							<option value="">Услуги центра</option>
							<option value="">Услуги центра</option>
							<option value="">Услуги центра</option>
							<option value="">Услуги центра</option>
							<option value="">Услуги центра</option>
						</select>
					</div>
					<div class="cell">
						<select name="">
							<option value="">Категория услуги</option>
							<option value="">Категория услуги</option>
							<option value="">Категория услуги</option>
							<option value="">Категория услуги</option>
						</select>
					</div>
					<div class="cell">
						<select name="">
							<option value="">Клиника</option>
							<option value="">Клиника</option>
							<option value="">Клиника</option>
							<option value="">Клиника</option>
						</select>
					</div>
					<div class="cell">
						<div class="switcher off"></div>
						<span>Для детей</span>
					</div>
					<div class="cell">
						<div class="button"><span>Поиск</span></div>
					</div>
				</div>
			</div>
			<div class="addictive-search">
				<div class="search">
					<input type="text" placeholder="Поиск услуги по названию">
				</div>
			</div>
			<div class="big-list-block">
				<div class="alphabet">
					<div class="search">
						<input type="text" placeholder="Поиск услуги...">
					</div>
					<div class="place">
						<div class="letters">
							<ul>
								<li><span>А</span></li>
								<li><span>Б</span></li>
								<li><span>В</span></li>
								<li class="active"><span>Г</span></li>
								<li><span>Д</span></li>
								<li><span>И</span></li>
								<li><span>К</span></li>
								<li><span>Л</span></li>
								<li><span>М</span></li>
								<li><span>Н</span></li>
								<li><span>О</span></li>
								<li><span>П</span></li>
								<li><span>Р</span></li>
								<li><span>С</span></li>
								<li><span>Т</span></li>
								<li><span>У</span></li>
								<li><span>Ф</span></li>
								<li><span>Х</span></li>
								<li><span>Э</span></li>
							</ul>
						</div>
						<div class="list scrollbar-outer">
							<ul>
								<li>Аллергология</li>
								<li>Анализы</li>
								<li>Аппаратная коррекция функциональных расстройств ЦНС</li>
								<li>Вакцинация</li>
								<li>Гастроэнтерология</li>
								<li>Гинекология</li>
								<li>Гирудотерапия</li>
								<li>Гомеопатия</li>
								<li>Дерматовенерология</li>
								<li>Детская поликлиника</li>
								<li>Диспансеризация</li>
								<li>Изготовление ортопедических стелек</li>
								<li>Интимная хирургия</li>
								<li>Кардиология</li>
								<li>Кардиовизор - контроль</li>
								<li>Косметология</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="result-block services-list">
					<div class="result-head">
						<h2>Аппаратная коррекция функциональных расстройств ЦНС</h2>
						<div class="count"><span>9</span><span>услуг</span></div>
					</div>
					<div class="result-list">
						<!-- Обрати внимание что класс item-а с префиксом s! -->
						<? for ($i=0; $i < 9; $i++) { ?>
						<div class="s-item">
							<div class="place">
								<div class="level-0">Детская поликлиника</div>
								<div class="level-1"><a href="#">Лазерный фракционный фототермолизна..</a></div>
								<div class="level-2"><a href="#">Подробнее</a></div>
							</div>
						</div>
						<?}?>
					</div>
					<div class="show-more">
						<div class="button">Показать больше</div>
					</div>
					<div class="paginator">
						<div class="to-start">
							<span>В начало</span>
						</div>
						<div class="pag">
							<div class="prev"></div>
							<ul>
								<li><a href="#">1</a></li>
								<li><a href="#" class="current">2</a></li>
								<li><a href="#">3</a></li>
							</ul>
							<div class="next"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- // -->
	
	<!-- // -->
	<? include 'inc/modules/seo-block.php';?>
</div>