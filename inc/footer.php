		<div class="footer">
			<div class="level-0">
				<div class="container">
					<div class="title white">Свяжитесь с нами</div>
					<div class="grid">
						<div class="cell-10 shift-1">
							<p>Воздухосодержание, как следует из вышесказанного, раскручивает комплексный контрпример. Измерение по определению упруго-пластично. Согласно последним исследованиям, выщелачивание притягивает гранулометрический анализ. </p>
						</div>
					</div>
					<div class="form">
						<form>
							<div class="group">
								<div class="cell">
									<input type="text" name="name" placeholder="Введите ваше имя">
								</div>
								<div class="cell">
									<input type="text" class="phone" name="phone" placeholder="Введите ваш телефон">
								</div>
								<div class="cell">
									<div class="btn">Отправить</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="level-1">
				<div class="container">
					<div class="adapt-phone">
						<div>
							<a href="tel:8465150-03-03">8 495 150-03-03</a>
						</div>
						<div>
							<a href="#" class="get-callback"><i class="icons-menu-callback"></i><span>Обратный звонок</span></a>
						</div>
					</div>
					<div class="grid menu-place">
						<div class="cell-2">
							<ul class="strong">
								<li><a href="#">Услуги</a></li>
								<li><a href="#">Цены</a></li>
								<li><a href="#">Врачи</a></li>
								<li><a href="#">Программы</a></li>
								<li><a href="#">Акции</a></li>
								<li><a href="#">Отзывы</a></li>
								<li><a href="#">Контакты</a></li>
							</ul>
						</div>
						<div class="cell-2">
							<ul>
								<li><a href="#">Пациентам</a></li>
								<li><a href="#">Вопрос врачу</a></li>
								<li><a href="#">Как стать пациентом?</a></li>
								<li><a href="#">Справочник заболеваний</a></li>
								<li><a href="#">Политика конфиденц.</a></li>
								<li><a href="#">О сайте</a></li>
								<li><a href="#">Документы</a></li>
							</ul>
						</div>
						<div class="cell-2">
							<ul>
								<li><a href="#">О компании</a></li>
								<li><a href="#">Контакты</a></li>
								<li><a href="#">Миссия</a></li>
								<li><a href="#">История</a></li>
								<li><a href="#">Пациентам</a></li>
								<li><a href="#">Отзывы</a></li>
								<li><a href="#">Вакансии</a></li>
							</ul>
						</div>
						<div class="cell-2">
							<ul>
								<li><a href="#">Пресс центр</a></li>
								<li><a href="#">Новости</a></li>
								<li><a href="#">Статьи</a></li>
								<li><a href="#">Социальная ответственность</a></li>
							</ul>
							<ul>
								<li><a href="#">Партнёрам</a></li>
								<li><a href="#">Партнёры</a></li>
								<li><a href="#">Как стать партнёров?</a></li>
							</ul>
						</div>
						<div class="cell-3 shift-1 phone">
							<div>
								<a href="tel:8465150-03-03">8 495 150-03-03</a>
							</div>
							<div>
								<a href="#" class="get-callback"><i class="icons-menu-callback"></i><span>Обратный звонок</span></a>
							</div>
						</div>
					</div>
					<div class="grid adress-place">
						<div class="cell-3">
							Наши клиники:
						</div>
						<a href="#" class="cell-3 adress">
							<div>ВЕРАМЕД Звенигород</div>
							<div>г. Звенигород, ул. Московская, 12</div>
						</a>
						<a href="#" class="cell-3 adress">
							<div>ВЕРАМЕД Премиум</div>
							<div>г. Одинцово, ул. Говорова, 18/1</div>
						</a>
						<a href="#" class="cell-3 adress">
							<div>ВЕРАМЕД Одинцово</div>
							<div>г. Одинцово, бул. Любы Новоселовой, 17</div>
						</a>
					</div>
				</div>
			</div>
			<div class="level-2">
				<div class="container">
					<div class="grid">
						<div class="cell-3 copy">
							<a href="/"><i class="icons-footer-logo"></i></a>
							<span>Copyright 2009-2016</span>
						</div>
						<div class="cell-9 info">Имеются противопоказания, необходима консультация специалиста</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>