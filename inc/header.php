<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>veramed</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

	<link rel="stylesheet" href="/project/css/style.min.css">

	<link rel="stylesheet" href="/project/js/lib/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="/project/js/lib/owl-carousel/owl.transitions.css">

	<script src="/project/js/lib/jquery-1.12.4.min.js"></script>

	<script src="/project/js/lib/jquery.formstyler.min.js"></script>

	<script src="/project/js/lib/owl-carousel/owl.carousel.min.js"></script>
	
	<script src="/project/js/lib/jquery.scrollbar/jquery.scrollbar.min.js"></script>
	<link rel="stylesheet" href="/project/js/lib/jquery.scrollbar/jquery.scrollbar.css">

	<script src="/project/js/lib/ui/jquery-ui.min.js"></script>
	<script src="/project/js/lib/icheck.min.js"></script>
	<script type="text/javascript" src="/project/js/lib/fancybox/jquery.fancybox.js"></script>
	<link rel="stylesheet" type="text/css" href="/project/js/lib/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
	
	<script src="/project/js/lib/jquery.validate.min.js"></script>
	<script src="/project/js/lib/jquery.maskedinput.min.js"></script>

	<script src="/project/js/fvalid.js"></script>
	<script src="/project/js/forms.js"></script>
	<script src="/project/js/main.js"></script>
</head>
<body>
	<div class="darkness"></div>
	<div class="grid-mask">
		<div class="container">
			<div class="grid">
				<div class="cell-1"></div>
				<div class="cell-1"></div>
				<div class="cell-1"></div>
				<div class="cell-1"></div>
				<div class="cell-1"></div>
				<div class="cell-1"></div>
				<div class="cell-1"></div>
				<div class="cell-1"></div>
				<div class="cell-1"></div>
				<div class="cell-1"></div>
				<div class="cell-1"></div>
				<div class="cell-1"></div>
			</div>
		</div>
	</div>
	<div class="fixed-adaptive-block">
		<div class="logo">
			<a href="/">
				<img src="/project/images/other/big-logo.png">
			</a>
		</div>
		<div class="right">
			<div class="sandwich">
				<i class="icons-sandwich"></i>
			</div>
		</div>
	</div>

	<div id="callback-form-place" class="modal-window">
		<i class="icons-close"></i>
		<div class="title-form">
			<div>Заказать обратный звонок</div>
		</div>
		<div class="form-place">
			<form id="modal-callback-form" novalidate="novalidate">
				<input type="hidden" name="form-title" value="Обратный звонок">
				<div class="field-name">
					<input type="text" placeholder="Ваше имя" name="name">
				</div>
				<div class="field-phone">
					<input type="text" placeholder="Ваш телефон" class="phone" name="phone">
				</div>      
				<div class="field-comment">
					<textarea name="comment" placeholder="Комментарий" class="valid"></textarea>
				</div>
				<div class="submit">
					<div class="btn">Отправить</div>
				</div>
			</form>
		</div>
	</div>

	<div id="resume-form-place" class="modal-window">
		<i class="icons-close"></i>
		<div class="title-form">
			<div>Связаться с отделом кадров</div>
		</div>
		<div class="form-place">
			<form id="modal-resume-form" novalidate="novalidate">
				<input type="hidden" name="form-title" value="Резюме">
				<div class="field-phone">
					<input type="text" placeholder="Телефон" class="phone" name="phone">
				</div>      
				<div class="field-name">
					<input type="text" placeholder="Имя" name="name">
				</div>
				<div class="field-comment">
					<textarea name="comment" placeholder="Комментарий" class="valid"></textarea>
				</div>
				<div class="add-resume">
					<div class="button">
						<span>Прикрепить резюме</span>
						<i class="icons-add-resume"></i>
					</div>
				</div>
				<div class="submit">
					<div class="btn">Отправить</div>
				</div>
			</form>
		</div>
	</div>

	<div id="register-form-place" class="modal-window">
		<i class="icons-close"></i>
		<div class="title-form">
			<div>Записаться на прием</div>
		</div>
		<div class="form-place">
			<form id="modal-register-form" novalidate="novalidate">
				<input type="hidden" name="form-title" value="Обратный звонок">
				<div class="group">
					<div class="cell">
						<input type="text" placeholder="ФИО" name="name">
					</div>
					<div class="cell">
						<input type="text" placeholder="Контактный телефон" class="phone" name="phone">
					</div>
					<div class="cell">
						<input type="text" placeholder="Электронная почта" name="email">
					</div>
					<div class="cell">
						<select name="">
							<option value="г. Одинцово, ул. Говорова, 18/1">г. Одинцово, ул. Говорова, 18/1</option>
							<option value="г. Одинцово, б-р. Любы Новоселовой, д. 17">г. Одинцово, б-р. Любы Новоселовой, д. 17</option>
							<option value="г. Звенигород, ул. Московская, д. 12">г. Звенигород, ул. Московская, д. 12</option>
							<option value="Не определился">Не определился</option>
						</select>
					</div>
					<div class="cell">
						<textarea name="comment" placeholder="Повод обращения" class="valid"></textarea>
					</div>
				</div>	
				<div class="submit">
					<div class="btn">Отправить</div>
				</div>
			</form>
		</div>
	</div>

	<div class="sandwich-menue">	
		<div class="search">
			<input type="text" placeholder="Поиск">
		</div>
		<ul>
			<li>
				<a href="#">Услуги</a>
				<ul>
					<li><a href="#">Все услуги</a></li>
					<li><a href="#">Анабиоз</a></li>
					<li><a href="#">Хирургия</a></li>
				</ul>
			</li>
			<li><a href="#">Цены</a></li>
			<li><a href="#">Врачи</a></li>
			<li><a href="#">Программы</a></li>
			<li><a href="#">Акции</a></li>
			<li><a href="#">Контакты</a></li>
			<li class="has-children">
				<a href="#">О Нас</a>
				<ul>
					<li><a href="#">О компании</a></li>
					<li><a href="#">Отзывы</a></li>
					<li><a href="#">Статьи</a></li>
					<li><a href="#">Новости</a></li>
					<li><a href="#">Помощь</a></li>
				</ul>
			</li>
		</ul>
		<div class="phone-block">
			<div class="phone"><a href="tel:8 495 150-03-03">8 495 150-03-03</a></div>
			<div class="button">
				<div class="btn get-callback"><i class="icons-red-phone"></i><span>Обратный звонок</span></div>
			</div>
		</div>
	</div>
	<div class="wrapper">
		<div class="top-info">
			<div class="container">
				<div class="grid">
					<div class="cell-10 shift-1">
						<div class="close"></div>
						<p>Lorem ipspm dolor sit amet, consectetur adipisicing elit. Molestias rerum, architecto consequatur adipisci praesentium nam, laudantium vero asperiores impedit omnis alias vel placeat aliquid, temporibus quaerat totam deserunt possimus amet blanditiis. Laboriosam nemo veritatis, praesentium asperiores provident facilis sunt, eaque ratione error amet a, eum commodi cupiditate libero nesciunt inventore sint blanditiis eveniet fuga! Repudiandae magnam repellat, dolor officia eum, eveniet non porro, totam consectetur vero ad perspiciatis tempora voluptatem asperiores at quaerat nobis dicta! Magni nobis quas at, temporibus accusantium velit voluptate sit adipisci possimus, ea fuga. Facere iure molestias optio voluptatibus ipsam maxime corporis dicta voluptatem hic laborum.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="header">
			<div class="level-0">
				<div class="container">
					<ul>
						<li><a href="#">О компании</a></li>
						<li><a href="#">Отзывы</a></li>
						<li><a href="#">Статьи</a></li>
						<li><a href="#">Новости</a></li>
						<li><a href="#">Помощь</a></li>
					</ul>
				</div>
			</div>
			<div class="level-1">
				<div class="container">
					<div class="grid">
						<div class="cell-2 logo">
							<a href="/"><i class="icons-main-logo"></i></a>
						</div>
						<div class="cell-8 menu">
							<div class="hidden-search">
								<input type="text" placeholder="Поиск...">
								<div class="btn red">Искать</div>
							</div>
							<div class="search">
								<i class="icons-search"></i><span>Поиск</span>
							</div>
							<ul>
								<li><a href="#">Услуги</a></li>
								<li><a href="#">Цены</a></li>
								<li class="active"><a href="#">Врачи</a></li>
								<li><a href="#">Программы</a></li>
								<li><a href="#">Акции</a></li>
								<li><a href="#">Контакты</a></li>
							</ul>
						</div>
						<div class="cell-2 phone">
							<div>
								<a href="tel:8495150-03-03">8 495 150-03-03</a>
							</div>
							<div>
								<a href="#" class="get-callback"><i class="icons-menu-callback"></i><span>Обратный звонок</span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>