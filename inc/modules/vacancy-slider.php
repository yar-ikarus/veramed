<div class="vac-list-slider">
	<div class="container">
		<div class="grid">
			<div class="cell-8 shift-2">
				<h2 class="title">Вакансии</h2>
				<div class="list-slider">
					<? for ($i=0; $i < 6; $i++) { ?>
					<div class="item">
						<div class="level-0">
							<span>28 августа 2016, 16:42</span>
						</div>
						<div class="level-1">
							<div class="name"><a href="#">Менеджер по продажам, менеджер по работе с клиентами</a></div>
							<div class="price">
								<span class="fix">25.000 - 50.000 Р</span>
							</div>
						</div>
						<div class="level-2">
							<div class="tags">
								<ul>
									<li>Акушер-гинеколог</li>
									<li>Уролог</li>
									<li>Перинаталог</li>
								</ul>
							</div>
						</div>
						<div class="level-3">
							<i class="icons-point"></i>
							<span><b>ВЕРАМЕД Одинцово</b> г. Одинцово, бул. Любы Новоселовой, 17</span>
						</div>
						<div class="level-4">
							<a href="#">Подробнее</a>
						</div>
					</div>
					<?}?>
				</div>
			</div>
		</div>
	</div>
</div>