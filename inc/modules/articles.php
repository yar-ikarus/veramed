<div class="articles">
	<div class="container">
		<h2 class="title">Статьи клиник</h2>
		<div class="grid">
			<div class="cell-8 shift-2">
				<div class="articles-slider">
					<? for ($i=0; $i < 6; $i++) { ?>
					<div class="item">
						<div class="level-0" style="background-image: url(/project/images/other/prog-1.jpg);">
							<div class="type">Статьи</div>
							<div class="name"><a href="#">Участвуй во всероссийском проекте «Проверь печень»</a></div>
						</div>
						<div class="level-1">«Первоочередной замысел всероссийского спецпроекта «Проверь печень» — подготовка и исследование данных о распространённости ... </div>
						<div class="level-2">
							<div class="tags">
								<ul>
									<li>Акушер-гинеколог</li>
									<li>Уролог</li>
									<li>Перинаталог</li>
									<li>Иглорефлексотерапевт</li>
								</ul>
							</div>
						</div>
						<div class="level-3">
							<a href="#">Подробнее</a>
						</div>
					</div>
					<?}?>
				</div>
			</div>
		</div>
	</div>
</div>