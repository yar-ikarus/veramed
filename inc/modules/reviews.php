<div class="reviews">
	<h2 class="title">Отзывы</h2>
	<div class="container">
		<div class="review-slider">
			<div class="item">
				<div class="group">
					<div class="cell">
						<div class="date">28 августа 2016, 16:42</div>
						<div class="tags">
							<ul>
								<li>Клиника в Одинцова</li>
								<li>Уролог</li>
								<li>Врач - Артюхов C.А.</li>
								<li>Урологические заболевания</li>
							</ul>
						</div>
						<div class="text">
							Уравнение малых колебаний относительно. Силовой трёхосный гироскопический стабилизатор стабилен. Точность крена позволяет исключить из рассмотрения лазерный ротор. Управление полётом самолёта активно...
						</div>
						<div class="show-more">
							<a href="#">Подробнее</a>
						</div>
					</div>
					<div class="cell">
						<div class="section">
							<div class="sec-title">
								Клиент:
							</div>
							<div class="name">
								<span><b>Коклюшевский</b> Константин Константинович</span>
							</div>
						</div>
						<div class="section client">
							<div class="sec-title">Отзыв врачу:</div>
							<div class="name">
								<div class="avatar" style="background-image: url(/project/images/other/avatar.jpg);"></div>
								<span><b>Сальникова</b> Ирина Александровна</span>
							</div>
						</div>
						<div class="all">
							<a href="#">Все отзывы</a>
						</div>
					</div>
				</div>
			</div>
			<div class="item">
				<div class="group">
					<div class="cell">
						<div class="date">28 августа 2016, 16:42</div>
						<div class="tags">
							<ul>
								<li>Клиника в Одинцова</li>
								<li>Уролог</li>
								<li>Врач - Артюхов C.А.</li>
								<li>Урологические заболевания</li>
							</ul>
						</div>
						<div class="text">
							Уравнение малых колебаний относительно. Силовой трёхосный гироскопический стабилизатор стабилен. Точность крена позволяет исключить из рассмотрения лазерный ротор. Управление полётом самолёта активно...
						</div>
						<div class="show-more">
							<a href="#">Подробнее</a>
						</div>
					</div>
					<div class="cell">
						<div class="section">
							<div class="sec-title">
								Клиент:
							</div>
							<div class="name">
								<span><b>Коклюшевский</b> Константин Константинович</span>
							</div>
						</div>
						<div class="section client">
							<div class="sec-title">Отзыв врачу:</div>
							<div class="name">
								<div class="avatar" style="background-image: url(/project/images/other/avatar.jpg);"></div>
								<span><b>Сальникова</b> Ирина Александровна</span>
							</div>
						</div>
						<div class="all">
							<a href="#">Все отзывы</a>
						</div>
					</div>
				</div>
			</div>
			<div class="item">
				<div class="group">
					<div class="cell">
						<div class="date">28 августа 2016, 16:42</div>
						<div class="tags">
							<ul>
								<li>Клиника в Одинцова</li>
								<li>Уролог</li>
								<li>Врач - Артюхов C.А.</li>
								<li>Урологические заболевания</li>
							</ul>
						</div>
						<div class="text">
							Уравнение малых колебаний относительно. Силовой трёхосный гироскопический стабилизатор стабилен. Точность крена позволяет исключить из рассмотрения лазерный ротор. Управление полётом самолёта активно...
						</div>
						<div class="show-more">
							<a href="#">Подробнее</a>
						</div>
					</div>
					<div class="cell">
						<div class="section">
							<div class="sec-title">
								Клиент:
							</div>
							<div class="name">
								<span><b>Коклюшевский</b> Константин Константинович</span>
							</div>
						</div>
						<div class="section client">
							<div class="sec-title">Отзыв врачу:</div>
							<div class="name">
								<div class="avatar" style="background-image: url(/project/images/other/avatar.jpg);"></div>
								<span><b>Сальникова</b> Ирина Александровна</span>
							</div>
						</div>
						<div class="all">
							<a href="#">Все отзывы</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>