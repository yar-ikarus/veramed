<div class="actions-block">
	<div class="container">
		<h2 class="title">Последние акции</h2>
		<div class="grid">
			<div class="cell-8 shift-2">
				<div class="slider-actions-block">
					<? for ($i=0; $i < 3; $i++) { ?>
					<div class="item" style="background-image: url(/project/images/other/action-bg-1.jpg);">
						<div class="level-0">Акции</div>
						<div class="level-1">
							<div class="name"><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium repel</a></div>
							<div class="date">с 19 сентября по 20 октября</div>
						</div>
						<div class="level-2">
							<div class="points">
								<div class="point">
									<span>ВЕРАМЕД</span>
									<span>Одинцово</span>
								</div>
								<div class="point">
									<span>ВЕРАМЕД</span>
									<span>Звенигород</span>
								</div>
								<div class="point">
									<span>ВЕРАМЕД</span>
									<span>Премиум</span>
								</div>
							</div>
							<div class="more">
								<a href="#">Подробнее</a>
							</div>
						</div>
					</div>
					<div class="item" style="background-image: url(/project/images/other/action-bg-1.jpg);">
						<div class="level-0">Акции</div>
						<div class="level-1">
							<div class="name"><a href="#">Маленький заголовок</a></div>
							<div class="date">с 19 сентября по 20 октября</div>
						</div>
						<div class="level-2">
							<div class="points">
								<div class="point">
									<span>ВЕРАМЕД</span>
									<span>Одинцово</span>
								</div>
								<div class="point">
									<span>ВЕРАМЕД</span>
									<span>Звенигород</span>
								</div>
								<div class="point">
									<span>ВЕРАМЕД</span>
									<span>Премиум</span>
								</div>
							</div>
							<div class="more">
								<a href="#">Подробнее</a>
							</div>
						</div>
					</div>
					<?}?>
				</div>
			</div>
		</div>
	</div>
</div>