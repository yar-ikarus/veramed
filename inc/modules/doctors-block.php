<div class="doctors-block">
	<div class="container">
		<h2 class="title">Прием ведут</h2>
		<div class="grid">
			<div class="cell-10 shift-1">
				<div class="doctors-slider">
					<a class="item" href="#">
						<div class="avatar" style="background-image: url(/project/images/other/doctor-1.jpg);"></div>
						<div class="profession">Акушер-гинеколог, перинатолог</div>
						<div class="name">
							<b>Ахмедова</b>
							<span>Шамала</span>
							<span>Акаевна</span>
						</div>
						<div class="adress">
							<span>г. Одинцово,бул ... ( 3 )</span>
						</div>
					</a>
					<a class="item" href="#">
						<div class="avatar" style="background-image: url(/project/images/other/doctor-2.jpg);"></div>
						<div class="profession">Аллерголог-Иммунолог</div>
						<div class="name">
							<b>Сиротина</b>
							<span>Алла</span>
							<span>Юрьевна</span>
						</div>
						<div class="adress">
							<span>г. Одинцово,бул ... ( 3 )</span>
						</div>
					</a>
					<a class="item" href="#">
						<div class="avatar" style="background-image: url(/project/images/other/doctor-3.jpg);"></div>
						<div class="profession">Иглорефлексотерапевт</div>
						<div class="name">
							<b>Абасова</b>
							<span>Аминат</span>
							<span>Зияудиновна</span>
						</div>
						<div class="adress">
							<span>г. Одинцово,бул ... ( 3 )</span>
						</div>
					</a>
					<a class="item" href="#">
						<div class="avatar" style="background-image: url(/project/images/other/doctor-4.jpg);"></div>
						<div class="profession">Оториноларинголог</div>
						<div class="name">
							<b>Аль-Хилали</b>
							<span>Александр </span>
							<span>Абдуллаевич</span>
						</div>
						<div class="adress">
							<span>г. Одинцово,бул ... ( 3 )</span>
						</div>
					</a>
					<a class="item" href="#">
						<div class="avatar" style="background-image: url(/project/images/other/doctor-3.jpg);"></div>
						<div class="profession">Иглорефлексотерапевт</div>
						<div class="name">
							<b>Абасова</b>
							<span>Аминат</span>
							<span>Зияудиновна</span>
						</div>
						<div class="adress">
							<span>г. Одинцово,бул ... ( 3 )</span>
						</div>
					</a>
					<a class="item" href="#">
						<div class="avatar" style="background-image: url(/project/images/other/doctor-4.jpg);"></div>
						<div class="profession">Оториноларинголог</div>
						<div class="name">
							<b>Аль-Хилали</b>
							<span>Александр </span>
							<span>Абдуллаевич</span>
						</div>
						<div class="adress">
							<span>г. Одинцово,бул ... ( 3 )</span>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>