<div class="where-block">
	<div class="container">
		<h2 class="title">Где принимают</h2>
		<div class="grid">
			<div class="cell-8">
				<div class="where-slider">
					<div class="item">
						<div class="level-0">
							<div class="name"><a href="#">ВЕРАМЕД Одинцово</a></div>
							<div class="info">
								<div class="phone">
									<a href="tel:8 (495) 150-34-87">8 (495) 150-34-87</a>
								</div>
								<div class="address">
									<span>г. Одинцово, б-р. Любы Новоселовой, д. 17</span>
								</div>
								<div class="time">
									<p><span>Пн-Сб:</span> <span>08:00 - 20:00;</span></p>
									<p><span>Вс:</span> <span>08:00 - 19:00</span></p>
								</div>
							</div>
							<div class="more">
								<a href="#">Подробнее</a>
							</div>
						</div>
						<div class="level-1">
							<div class="map" style="background-image: url(/project/images/other/map-1.jpg);">
								<a href="#"></a>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="level-0">
							<div class="name"><a href="#">ВЕРАМЕД Премиум</a></div>
							<div class="info">
								<div class="phone">
									<a href="tel:8 (495) 150-34-87">8 (495) 150-34-87</a>
								</div>
								<div class="address">
									<span>г. Одинцово, ул. Говорова, 18/1</span>
								</div>
								<div class="time">
									<p><span>Ежедневно</span> <span>08:00 - 20:00</span></p>
								</div>
							</div>
							<div class="more">
								<a href="#">Подробнее</a>
							</div>
						</div>
						<div class="level-1">
							<div class="map" style="background-image: url(/project/images/other/map-2.jpg);">
								<a href="#"></a>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="level-0">
							<div class="name"><a href="#">ВЕРАМЕД Звенигород</a></div>
							<div class="info">
								<div class="phone">
									<a href="tel:8 (495) 150-03-03">8 (495) 150-03-03</a>
								</div>
								<div class="address">
									<span>г. Звенигород, ул. Московская, д. 12</span>
								</div>
								<div class="time">
									<p><span>Ежедневно</span> <span>08:00 - 19:00</span></p>
								</div>
							</div>
							<div class="more">
								<a href="#">Подробнее</a>
							</div>
						</div>
						<div class="level-1">
							<div class="map" style="background-image: url(/project/images/other/map-3.jpg);">
								<a href="#"></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>