<div class="all-news-slider">
	<div class="container">
		<h2 class="title">Новости клиник</h2>
		<div class="list">
			<? for ($i=0; $i < 3; $i++) { ?>
			<div class="item">
				<div class="place">
					<div class="level-0">1 сентября 2016, 08:15</div>
					<div class="level-1"><a href="#">Эпидсезон по вирусному энцефалиту начался в России</a></div>
					<div class="level-2">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut...</div>
					<div class="level-3"><a href="#">Подробнее</a></div>
				</div>
			</div>
			<div class="item">
				<div class="place">
					<div class="level-0">1 сентября 2016, 08:15</div>
					<div class="level-1"><a href="#">С Днем медицинского работника!</a></div>
					<div class="level-2">Красоту женских ног всегда воспевали поэты...</div>
					<div class="level-3"><a href="#">Подробнее</a></div>
				</div>
			</div>
			<?}?>
		</div>			
	</div>
</div>